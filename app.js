var express          = require('express')
var app              = express()
var path             = require('path');
var multer           = require('multer');
var mysql            = require('mysql');
var bodyParser       = require('body-parser')
global.nombre_imagen = require('underscore')
global.update_imagen = require('underscore')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var storage = multer.diskStorage({
  destination: './public/uploads/',
  filename: function (req, file, cb) {
    cb(null, file.originalname.replace(path.extname(file.originalname), "") + '-' + Date.now() + path.extname(file.originalname))
  }
})

var upload = multer({ storage: storage })

app.get('/', function(req, res){
 res.sendFile(__dirname + '/client/views/index.html')
})

app.use(express.static(__dirname + '/public'))

app.post('/test', upload.single('file'), function(req,res,next){
    console.log('Uploade Successful ', req.file, req.body);
    res.json({err:false})
});

app.get('/init', function(req, res){
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'angel'
  });

  connection.connect();

  connection.query(
    `SELECT
      *
     FROM
      user
    `, function (error, results, fields) {
    if (error) {
      throw error
    } else {
      res.json(results)
    }
  });

  connection.end();
})

app.post('/set-car', upload.single('file'), function(req,res,next){
    console.log('Uploade Successful ', req.file, req.body);
    global.nombre_imagen = req.file.filename
    res.json({err:false})
});

app.post('/set-car-name', function(req, res){
  var d = req.body
  var dire = 'http://localhost:3000/uploads/' + global.nombre_imagen
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'angel'
  });

  connection.connect();

  connection.query(
    `
      INSERT INTO
        user (nombre, imagen)
      VALUES (?, ?)
    `, [d.nombre, dire], function (error, results, fields) {
    if (error) {
      throw error
    } else {
      res.json({err:false, info: {nombre:d.nombre, imagen: dire}})
    }
  });

  connection.end();
})

app.post('/do-eliminar', function(req, res){
  var d = req.body
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'angel'
  });

  connection.connect();

  connection.query(
    `
      DELETE FROM
        user
      WHERE
        id = ?
    `, [d.id], function (error, results, fields) {
    if (error) {
      throw error
    } else {
      res.json({err:false})
    }
  });

  connection.end();
})

app.post('/update-car', upload.single('file'), function(req,res,next){
    console.log('Uploade Successful ', req.file, req.body);
    global.update_imagen = req.file.filename
    res.json({err:false})
});

app.post('/update-car-name', function(req, res){
  var d = req.body
  var dire = 'http://localhost:3000/uploads/' + global.update_imagen
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'angel'
  });

  connection.connect();

  connection.query(
    `
      UPDATE
        user
      SET
        nombre = ?,
        imagen = ?
      WHERE
        id = ?
    `, [d.nombre,dire,d.id], function (error, results, fields) {
    if (error) {
      throw error
    } else {
      res.json({err:false, info:{id:d.id,nombre:d.nombre,imagen:dire}})
    }
  });

  connection.end();
})

app.post('/update-car-item', function(req, res){
  var d = req.body
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'angel'
  });

  connection.connect();

  connection.query(
    `
      UPDATE
        user
      SET
        nombre = ?
      WHERE
        id = ?
    `, [d.nombre,d.id], function (error, results, fields) {
    if (error) {
      throw error
    } else {
      res.json({err:false})
    }
  });

  connection.end();
})

app.listen(3000, function(){
 console.log('Escuchando en el puerto 3000')
})
